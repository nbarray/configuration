" Desactivate vi compatibility
set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle
call vundle#begin()

" Let Vundle manage Vundle
Plugin 'gmarik/vundle'

" My Bundles
Plugin 'bling/vim-airline'
Plugin 'tpope/vim-fugitive'
Plugin 'DoxygenToolkit.vim'
Plugin 'kien/ctrlp.vim'

call vundle#end()

filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""
" PLUGIN configuration section
"""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""" Vim-airline

" Display vim airline even if no split is done
set laststatus=2

" Better fonts
let g:airline_powerline_fonts=1

" Better tabs
let g:airline#extensions#tabline#enable=1

" No F***K .swp files, git exists for that
set noswapfile

" Activate syntaxic coloration

set t_Co=256
syntax on
colorscheme jellybeans

" Activate aside line nnumbers
set number

" Correct indentation
set tabstop=4
set expandtab
set shiftwidth=4
set smartindent
set autoindent

" Ignore case for search
set ignorecase

" Show edition mode
set showmode

" Show the ruler at the bottom side
set ruler

set hlsearch

"""""""""""""""""""""""""""""""""""
" Mapping
"""""""""""""""""""""""""""""""""""

map <C-b> :bNext<CR>
